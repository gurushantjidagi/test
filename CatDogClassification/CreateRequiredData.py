import os,shutil

original_dataset_dir="data/train/"

base_dir="data/cats_and_dogs_small"

os.mkdir(base_dir)

train_dir=os.path.join(base_dir,"train")
os.mkdir(train_dir)

test_dir=os.path.join(base_dir,"test")
os.mkdir(test_dir)

validation_dir=os.path.join(base_dir,"validation")
os.mkdir(validation_dir)

cat_train=os.path.join(train_dir,"cats")
os.mkdir(cat_train)

dog_train=os.path.join(train_dir,"dogs")
os.mkdir(dog_train)

cat_validation=os.path.join(validation_dir,"cats")
os.mkdir(cat_validation)

dog_validation=os.path.join(validation_dir,"dogs")
os.mkdir(dog_validation)

cat_test=os.path.join(test_dir,"cats")
os.mkdir(cat_test)

dog_test=os.path.join(test_dir,"dogs")
os.mkdir(dog_test)

fnames=['cat.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
    src=os.path.join(original_dataset_dir,fname)
    dst=os.path.join(cat_train,fname)
    shutil.copy(src,dst)

fnames=['cat.{}.jpg'.format(i) for i in range(1000,1500)]
for fname in fnames:
    src=os.path.join(original_dataset_dir,fname)
    dst=os.path.join(cat_validation,fname)
    shutil.copy(src,dst)

fnames=['cat.{}.jpg'.format(i) for i in range(1500,2000)]
for fname in fnames:
    src=os.path.join(original_dataset_dir,fname)
    dst=os.path.join(cat_test,fname)
    shutil.copy(src,dst)

#####for dog
fnames=['dog.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
    src=os.path.join(original_dataset_dir,fname)
    dst=os.path.join(dog_train,fname)
    shutil.copy(src,dst)

fnames=['dog.{}.jpg'.format(i) for i in range(1000,1500)]
for fname in fnames:
    src=os.path.join(original_dataset_dir,fname)
    dst=os.path.join(dog_validation,fname)
    shutil.copy(src,dst)

fnames=['dog.{}.jpg'.format(i) for i in range(1500,2000)]
for fname in fnames:
    src=os.path.join(original_dataset_dir,fname)
    dst=os.path.join(dog_test,fname)
    shutil.copy(src,dst)

print (len(os.listdir(cat_train)))
print (len(os.listdir(cat_validation)))
print (len(os.listdir(cat_test)))

print (len(os.listdir(dog_train)))
print (len(os.listdir(dog_validation)))
print (len(os.listdir(dog_test)))